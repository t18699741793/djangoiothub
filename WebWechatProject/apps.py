from django.apps import AppConfig


class WebwechatprojectConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'WebWechatProject'

    def ready(self):
        from WebWechatProject.services import get_mqtt
        mqtt = get_mqtt()

        mqtt.connect('192.168.227.59')
        mqtt.start_loop()
