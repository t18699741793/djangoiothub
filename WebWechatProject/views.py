from django.http import HttpResponse
from django.core.paginator import Paginator
from django.shortcuts import render
from json import dumps
from django.db.models import Max

from WebWechatProject.services import get_mqtt
from WebWechatProject.forms import UpdateStateForm
from WebWechatProject.models import TemperatureMeasures
from WebWechatProject.models import HumidityMeasures
from WebWechatProject.models import DeviceActivationLog
from django.contrib.auth.decorators import login_required

from django.shortcuts import render
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import serializers


# Create your views here.

# @login_required
def actuators(request):
    # return render(request, 'webApp/actuators.html')
    # relays_list = DeviceActivationLog.objects.all()
    relays_list = DeviceActivationLog.objects.all().order_by('created_at')
    # relay_latest = DeviceActivationLog.objects.all().aggregate(Max('id'))
    # print(f"MEASURE {measure}")
    relays = relays_list.filter().last()
    # print(relays.measure)
    form = UpdateStateForm()
    context = {
        'relays_list': relays,
        'title': 'Available Relays',
        'form': form
    }
    return render(request, 'webApp/index.html', context)


def blink_led_on(request):
    mqtt = get_mqtt()

    mqtt.publish_message('Lijiang/webApp/actuator', 'true')

    return HttpResponse(status=200)


def blink_led_off(request):
    mqtt = get_mqtt()

    mqtt.publish_message('Lijiang/webApp/actuator', 'false')

    return HttpResponse(status=200)


def temperature_measures(request):
    temperature_measures_list = TemperatureMeasures.objects.all().order_by('-created_at')

    paginator = Paginator(temperature_measures_list, 30)
    page = request.GET.get('page')
    measures = paginator.get_page(page)

    return render(request, 'webApp/temperature_measures.html', {'temperature_measures': measures})


def humidity_measures(request):
    humidity_measures_list = HumidityMeasures.objects.all().order_by('-created_at')

    paginator = Paginator(humidity_measures_list, 30)
    page = request.GET.get('page')
    measures = paginator.get_page(page)

    return render(request, 'webApp/humidity_measures.html', {'humidity_measures': measures})


def temperature_chart(request):
    temperature_measures = TemperatureMeasures.objects.all().order_by('-created_at')[:50]

    temperatures = [float(temperature_measure.measure) for temperature_measure in temperature_measures]
    times = [temperature_measure.created_at.strftime('%Y-%m-%d %H:%M:%S') for temperature_measure in
             temperature_measures]

    context = {
        'temperatures': dumps(temperatures),
        'times': dumps(times)
    }

    return render(request, 'webApp/temperature_chart.html', context)


def humidity_chart(request):
    humidity_measures = HumidityMeasures.objects.all().order_by('-created_at')[:50]

    humidities = [float(humidity_measure.measure) for humidity_measure in humidity_measures]
    times = [humidity_measure.created_at.strftime('%Y-%m-%d %H:%M:%S') for humidity_measure in
             humidity_measures]

    context = {
        'humidities': dumps(humidities),
        'times': dumps(times)
    }

    return render(request, 'webApp/humidity_chart.html', context)


"""微信小程序部分"""


class BlinkMeasuresView(APIView):
    """控制LED灯"""

    def get(self, request, format=None):
        Blink = DeviceActivationLog.objects.all()
        serializer = BlinkMeasureSerializer(Blink, many=True)
        print(serializer.data)
        return Response(serializer.data)

    def post(self, request, format=None):
        mqtt = get_mqtt()
        received_data = request.data  # 获取客户端发送的数据
        if received_data['status'] == 'open':
            mqtt.publish_message('Lijiang/webApp/actuator', 'true')
        elif received_data['status'] == 'close':
            mqtt.publish_message('Lijiang/webApp/actuator', 'false')
        return Response({"message": "Data received and processed successfully"})


class TemperatureMeasuresView(APIView):
    """温度视图（返回温度数据）"""

    def get(self, request, format=None):
        temperature = TemperatureMeasures.objects.all()
        serializer = TemperatureMeasureSerializer(temperature, many=True)
        print(serializer.data)
        return Response(serializer.data)

    def post(self, request, format=None):
        pass


class HumidityMeasuresView(APIView):
    """湿度视图（返回湿度数据）"""

    def get(self, request, format=None):
        humidity = HumidityMeasures.objects.all()
        serializer = HumidityMeasureSerializer(humidity, many=True)
        print(serializer.data)
        return Response(serializer.data)

    def post(self, request, format=None):
        pass


class TemperatureMeasureSerializer(serializers.ModelSerializer):
    """TemperatureMeasures 模型转换为 JSON 格式"""
    created_at = serializers.DateTimeField(format="%Y-%m-%d %H:%M:%S")

    class Meta:
        model = TemperatureMeasures
        fields = ['id', 'created_at', 'measure']


class HumidityMeasureSerializer(serializers.ModelSerializer):
    """HumidityMeasures 模型转换为 JSON 格式"""
    created_at = serializers.DateTimeField(format="%Y-%m-%d %H:%M:%S")

    class Meta:
        model = HumidityMeasures
        fields = ['id', 'created_at', 'measure']


class BlinkMeasureSerializer(serializers.ModelSerializer):
    """DeviceActivationLog 模型转换为 JSON 格式"""
    created_at = serializers.DateTimeField(format="%Y-%m-%d %H:%M:%S")

    class Meta:
        model = DeviceActivationLog
        fields = ['id', 'created_at', 'measure']
