# DjangoIoTHub



## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd djangoiothub
git remote add origin https://Lijiang:glpat-ynY9nwFamFtQ1xajaGcY@gitlab.com/t18699741793/djangoiothub.git
git branch -M main
git push -uf origin main
```

## 项目介绍：

该项目是基于Django开发的一个简单物联网平台，包括网页端和微信小程序端，两者功能相同。主要用于展示温湿度数据，并提供折线图和一个开关功能。

项目后端使用Django框架，通过mqtt协议实现数据的采集和传输。数据可以是从传感器采集到的实时数据，也可以是模拟生成的随机数据。项目中包含了订阅段，一旦接收到信息，立即将数据保存到MySQL数据库中。

微信小程序通过访问Django提供的API来获取数据，并在相应的页面中展示。小程序端同样具备折线图和开关功能。此外，项目还可作为发布端使用，通过开关来执行发布的消息。例如，在某个装有ESP32板子的设备上运行mqtt的订阅段代码，当点击打开按钮时，LED灯会亮起；点击关闭按钮时，LED灯会熄灭。

小程序端还提供了一个模拟发送数据的功能，这时它扮演着mqtt协议中的发布端。用户进入模拟发送数据界面后，设置主题名并填写相应的数据，然后点击发送按钮将数据http协议来发布。Django项目会接收到发布的消息，并通过mqtt形式进行发布端处理。另一个进程会接收到发布的消息，然后将数据保存到数据库中。

该物联网平台基于Django开发，结合mqtt协议和MySQL数据库实现了温湿度数据的展示、折线图功能以及开关控制。通过微信小程序端和网页端的使用，用户可以方便地监控温湿度数据，并通过模拟发送数据功能进行测试和发布控制命令。



微信小程序端地址：https://gitlab.com/t18699741793/weconnect-iot
